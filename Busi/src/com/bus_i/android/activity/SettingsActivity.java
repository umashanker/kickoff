package com.bus_i.android.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

import com.bus_i.android.R;

/**
 * 
 * @author Uma Shanker
 *
 */
public class SettingsActivity extends PreferenceActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
		SharedPreferences prefs =
				PreferenceManager.getDefaultSharedPreferences(this);

		SharedPreferences.OnSharedPreferenceChangeListener listener =
				new SharedPreferences.OnSharedPreferenceChangeListener() {
			public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
				Preference pref = findPreference(key);
				if (key.equals(getString(R.string.refresh_interval_min_key))) {
					// Show selected value
					pref.setSummary(sharedPreferences.getString(key, "5") + " minutes");
				}
			}
		};
		prefs.registerOnSharedPreferenceChangeListener(listener);
	}
}
