package com.bus_i.android.activity;


import java.util.List;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.ShareActionProvider;

import com.bus_i.android.R;
import com.bus_i.android.asynctasks.CommonAsyncTask;
import com.bus_i.android.constants.WebConstants;
import com.bus_i.android.fragment.MapViewFragment;
import com.bus_i.android.interfaces.AsyncTaskDelegate;
import com.bus_i.android.interfaces.SetMapRootInterface;
import com.bus_i.android.interfaces.SetMyLocationClickLstener;
import com.bus_i.android.manager.ParsingManager;
import com.bus_i.android.manager.PreferenceManager;
import com.bus_i.android.models.LocateBus;
import com.bus_i.android.utills.GPSTracker;
import com.bus_i.android.utills.InternetUtil;
import com.bus_i.android.utills.MessageAlert;

/**
 * 
 * @author Uma Shanker
 *
 */
public class MainActivity extends FragmentActivity implements AsyncTaskDelegate{


	private boolean mMyLocation=true;
	private CommonAsyncTask task;
	private Fragment mapFragment;
	private String busName = null; 
	private MenuItem locatioItem;
	private LocateBus bus = null;
	private GPSTracker gps;

	/**
	 * Initialize view 
	 */
	private void initView(){
		init();
		// this will load the last search bus name if it is not null
		if(PreferenceManager.getInstance(this).getSearchedBusName() != null){
			searchRunningBus(PreferenceManager.getInstance(this).getSearchedBusName());
		}
		if(PreferenceManager.getInstance(this).getappLaunchedFirstTime()){
			showChalkHelp();
			PreferenceManager.getInstance(this).setappLaunchedFirstTime(false);
		}
	}

	/**
	 * Initialize the componant of activity
	 */
	private void init(){
		mapFragment=getSupportFragmentManager().findFragmentByTag("MapFragment");
		if(mapFragment==null){
			mapFragment=new MapViewFragment();
		}
		getSupportFragmentManager()
		.beginTransaction()
		.add(R.id.container, mapFragment,"MapFragment")
		.commit();
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main); 
		initView();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		MenuItem shareItem = menu.findItem(R.id.action_share);
		locatioItem = menu.findItem(R.id.action_my_location);
		ShareActionProvider shareAction = (ShareActionProvider)shareItem.getActionProvider();
		shareAction.setShareIntent(createShareIntent());

		searchItem(menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * This is for serach controll of Action bar it  will execute async task after task completion
	 */
	private void searchItem(Menu menu) {

		final SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
		if (null != searchView )
		{
			searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
			searchView.setIconifiedByDefault(false);   
		}
		searchView.setOnQueryTextListener(queryTextListener);
	}

	/**
	 * Query Text Listener class
	 */
	SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() 
	{
		public boolean onQueryTextChange(final String newText) 
		{

			return true;
		}

		public boolean onQueryTextSubmit(final String query) 
		{
			busName = query;
			searchRunningBus(busName);
			InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE); 
			inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
			getCurrentFocus().clearFocus();
			return true;
		}

	};

	/**
	 * search bus using bus name and return on map
	 * @param query
	 */
	private void searchRunningBus(final String query) {
		if(InternetUtil.isInternetOn(this)){
			task=new CommonAsyncTask(MainActivity.this,this, false);
			String url=WebConstants.LOCATE_BUS+"cmd=locate&code="+query;
			task.execute(url);

			setProgressBarIndeterminateVisibility(true);
		}
		else{
			InternetUtil.showErrorDialog(this, false);
		}
	}

	/**
	 * create share intent 
	 */
	protected Intent createShareIntent() {

		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		PackageManager pm = this.getPackageManager();
		List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Bus-i");
		for(final ResolveInfo app : activityList) {
			if("com.facebook.katana.ShareLinkActivity".equals(app.activityInfo.name)) {
				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, " http://bus-i.com/track/");
			} else {
				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.share_text));
			}
		}
		return shareIntent;
	}

	// Action bar menu handler
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {

		case R.id.action_my_location:
			GPSTracker	gps = new GPSTracker(this);
			if(mMyLocation){
				busAndUserLocationNavigator(item);
			}
			else if(!mMyLocation && gps.canGetLocation()){
				busAndUserLocationNavigator(item);
			}else{
				gps.showSettingsAlert();
			}

			return true;
		case R.id.action_alarm:
			MessageAlert.showGeneralAlert(MainActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.alarm_procedure), getResources().getDrawable(R.drawable.ic_bus_marker), false);
			return true;
		case R.id.action_settings:
			// open Settings
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;

		case R.id.action_help:
			// reset help shown status to display overlay help
			//	mEditor.putBoolean("CHALK_HELP_SHOWN", false).commit();
			showChalkHelp();
			//	MessageAlert.showGeneralAlert(MainActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.toast_help), getResources().getDrawable(R.drawable.ic_bus_marker), false);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * this methid is used to show user location and bus location if bus is selected
	 * @param item
	 */
	private void busAndUserLocationNavigator(MenuItem item) {
		Fragment f=getSupportFragmentManager().findFragmentByTag("MapFragment");
		SetMyLocationClickLstener listener=(SetMyLocationClickLstener) f;
		if(mMyLocation) {
			if(bus != null && bus.getStatus().equals("success")){
				listener.setIsMyLocation(mMyLocation);
				item.setIcon(R.drawable.ic_action_my_location);
				mMyLocation = false;
			}
			else{
				MessageAlert.showGeneralAlert(this, getResources().getString(R.string.alert),getResources().getString(R.string.bus_not_found), getResources().getDrawable(R.drawable.ic_launcher), false);
			}
		} else {

			listener.setIsMyLocation(mMyLocation);
			item.setIcon(R.drawable.ic_action_bus_location);
			mMyLocation = true;
		}
	}

	/**
	 * Set response from async task response
	 * @see com.bus_i.android.interfaces.AsyncTaskDelegate#setResponse(java.lang.String)
	 */
	@Override
	public void setResponse(LocateBus response) {
		setProgressBarIndeterminateVisibility(false);
		Fragment f=getSupportFragmentManager().findFragmentByTag("MapFragment");
		SetMapRootInterface inter=(SetMapRootInterface) f;
		if(response !=null){
			bus = response;//ParsingManager.doParsing(response, LocateBus.class);
			if(bus != null && !bus.getStatus().equals("error")){
				if(busName != null){
					PreferenceManager.getInstance(MainActivity.this).setSearchedBusName(busName);	
				}

				locatioItem.setIcon(R.drawable.ic_action_my_location);
				mMyLocation = false;
			}
			else{
				locatioItem.setIcon(R.drawable.ic_action_bus_location);
				mMyLocation = true;
				//		MessageAlert.showGeneralAlert(MainActivity.this, getResources().getString(R.string.alert), bus.getMessage(), getResources().getDrawable(R.drawable.ic_launcher), false);
			}
			inter.setMapRoot(bus);
		}
	}

	// show overlay help chalk screen
	public void showChalkHelp(){

		final View masterView = (RelativeLayout)findViewById(R.id.chalkHelpLayout);
		masterView.setVisibility(View.VISIBLE);
		masterView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(findViewById(R.id.imageChalkBottom).getVisibility() != View.VISIBLE){
					findViewById(R.id.imageChalkBottom).setVisibility(View.VISIBLE);
					findViewById(R.id.textChalkLastPosition).setVisibility(View.VISIBLE);
					findViewById(R.id.textChalkNextStop).setVisibility(View.VISIBLE);
					findViewById(R.id.imageChalkTopRight).setVisibility(View.INVISIBLE);
					findViewById(R.id.textChalkMenu).setVisibility(View.INVISIBLE);
					findViewById(R.id.textChalkOverflow).setVisibility(View.INVISIBLE);
				} else {
					findViewById(R.id.imageChalkBottom).setVisibility(View.INVISIBLE);
					findViewById(R.id.textChalkLastPosition).setVisibility(View.INVISIBLE);
					findViewById(R.id.textChalkNextStop).setVisibility(View.INVISIBLE);
					findViewById(R.id.imageChalkTopRight).setVisibility(View.VISIBLE);
					findViewById(R.id.textChalkMenu).setVisibility(View.VISIBLE);
					findViewById(R.id.textChalkOverflow).setVisibility(View.VISIBLE);
					masterView.setVisibility(View.GONE);

				}
			}
		});

	}
@Override
protected void onStop() {
	if(task != null && task.getStatus() == AsyncTask.Status.RUNNING){
		task.cancel(true);
	}
	super.onStop();
}
@Override
protected void onPause() {
	if(task != null && task.getStatus() == AsyncTask.Status.RUNNING){
		task.cancel(true);
	}
	super.onPause();
}
}
