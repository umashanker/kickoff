package com.bus_i.android.interfaces;

import com.bus_i.android.models.LocateBus;

/**
 * 
 * @author Uma Shanker
 *
 */
public interface AsyncTaskDelegate {
	
	public void setResponse(LocateBus response);


}
