package com.bus_i.android.adapter;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.Fragment;
import android.content.Context;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bus_i.android.R;
import com.bus_i.android.models.LocateBus;
import com.bus_i.android.models.Stop;

public class RouteListAdapter extends BaseAdapter{

	private List<Stop> routeInfoList;
	private Context mContext;


	public RouteListAdapter(List<Stop> routeList, Context context){
		this.routeInfoList =routeList;

		mContext = context;
	}
	@Override
	public int getCount() {
		return routeInfoList.size();
	}

	@Override
	public Object getItem(int arg0) {
		return routeInfoList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int pos, View contentView, ViewGroup arg2) {
		Holder holder;
		final Stop route = (Stop)getItem(pos);

		if(contentView == null){
			LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			contentView = inflater.inflate(R.layout.rout_list_item, null);
			holder = new Holder();
			holder.rootListStopsTextView = (TextView)contentView.findViewById(R.id.tv_rout_list_stops);

			contentView.setTag(holder);
		}else{
			holder = (Holder)contentView.getTag();
		}
		String time="";
		String address = "";
		try{
			time =getTime(route.getTimestamp());
			address = (route.getAddress().getLine1() + " " + route.getAddress().getLine2()).replace("null", "");

		}catch(NullPointerException e){
			e.printStackTrace();
		}
		holder.rootListStopsTextView.setText("At " + time +", " + address);
		return contentView;
	}

	public class Holder{
		TextView rootListStopsTextView;

	}

	/**
	 * get time from timestamp
	 * @param time
	 * @return
	 */
	private String getTime(long time) {
		Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		cal.setTimeInMillis(time);
		String ltime = DateFormat.format("hh:mm a", cal).toString();
		return ltime;
	}
}
