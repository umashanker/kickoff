package com.bus_i.android.services;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.bus_i.android.R;
import com.bus_i.android.activity.MainActivity;


public class WidgetProvider extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {

    
        // initializing widget layout
        RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.widget_1x);

        // obtain intent and set onclick of widget to open main app
        Intent launchAppIntent = new Intent(context, MainActivity.class);
        PendingIntent launchAppPendingIntent = PendingIntent.getActivity(context,
                0, launchAppIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteView.setOnClickPendingIntent(R.id.widgetLayout, launchAppPendingIntent);

        // todo Populate tracking info in widget
        // remoteView.setTextViewText(R.id.statusTitle, "Testing");

        // request for widget update
        pushWidgetUpdate(context, remoteView);
    }

    public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
        ComponentName myWidget = new ComponentName(context,
                WidgetProvider.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        manager.updateAppWidget(myWidget, remoteViews);
    }
}
