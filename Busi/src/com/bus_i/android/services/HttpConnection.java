package com.bus_i.android.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.util.Log;

public class HttpConnection {


	public static String doPost(String body,String url) throws ClientProtocolException, IOException{
		String httpResponse = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpPost httpPut = new HttpPost(url);
		if (body != null)
			try {
				httpPut.setEntity(new StringEntity(body, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				System.out.println("UnsupportedEncodingException");
				e.printStackTrace();
			}
		httpPut.setHeader("Content-type", "application/json");
		HttpResponse response = null;

		response = httpClient.execute(httpPut, localContext);
		Log.e("Response code",""+response.getStatusLine().getStatusCode());
		HttpEntity entity = response.getEntity();
		httpResponse=EntityUtils.toString(entity);//getStringFromEntity(entity);

		return httpResponse;
	}

	public static String doGet(String url,Context context) throws ParseException, IOException,ClientProtocolException{	
		String httpResponse = null;	
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("Content-type", "application/json");
		HttpResponse response = null;
		response = httpClient.execute(httpGet);
		//Log.e("Response code of global",""+response.getStatusLine().getStatusCode());
		HttpEntity entity = response.getEntity();
		httpResponse=EntityUtils.toString(entity);//getStringFromEntity(entity);
		//Log.e("Response  of global",httpResponse);
		return httpResponse;
	}



}
