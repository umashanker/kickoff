package com.bus_i.android.asynctasks;

import java.io.IOException;

import org.apache.http.ParseException;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.bus_i.android.R;
import com.bus_i.android.interfaces.AsyncTaskDelegate;
import com.bus_i.android.manager.ParsingManager;
import com.bus_i.android.models.LocateBus;
import com.bus_i.android.services.HttpConnection;
import com.bus_i.android.utills.InternetUtil;

public class CommonAsyncTask extends AsyncTask<String, Void, LocateBus> {

	private LocateBus response = null;
	private Exception exception;
	private Context context;
	private AsyncTaskDelegate delegate;
	ProgressDialog dialog;
	Boolean isLoader;

	public CommonAsyncTask(Context context,AsyncTaskDelegate deligate, Boolean isLoader){
		this.context=context;
		this.delegate=deligate;
		this.isLoader=isLoader;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if(isLoader){
			dialog=ProgressDialog.show(context, "", context.getResources().getString(R.string.Label_Loading),false);
		}
	}

	@Override
	protected LocateBus doInBackground(String...url) {

		if(url[0]!=null){
			try {
				response=ParsingManager.doParsing(HttpConnection.doGet(url[0],context),LocateBus.class);
			} catch (ParseException e) {
				exception=e;
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				exception=e;
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return response;
	}

	@Override
	protected void onPostExecute(LocateBus result) {
		super.onPostExecute(result);

		if (exception == null) {
			delegate.setResponse(result);
		} else {
		//	InternetUtil.showErrorDialog(context,true);
			exception.printStackTrace();
		}
		if(isLoader){
			dialog.dismiss();
		}
	}


}
