package com.bus_i.android.constants;

public class PreferenceManagerConstants {
public static final String SETTING_PREFERENCE="settings";
public static final String DEFAULT_APP_MODE = "appMode";

public static final String LAST_SEARCHED_BUS_NAME = "lastSearchedBusName";
public static final String IS_APP_LAUNCHED_FIRST_TIME = "appLaunched";
}
