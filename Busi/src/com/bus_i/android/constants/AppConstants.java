package com.bus_i.android.constants;

public class AppConstants {
	public static final String IS_MAP = "map";

	//Marker Name Constant
	public static String MARKER_ORIGIN = "originMarker"; 
	public static String MARKER_DESTINATION = "destinationMarker";
	public static String MARKER_ALERM = "alermMarker";
	public static String MARKER_CURRENT_POSITION = "currentLocationMarker";
	public static String MARKER_USER_LOCATION = "userLocationMarker";
}