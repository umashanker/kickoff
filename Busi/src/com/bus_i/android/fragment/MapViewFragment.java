package com.bus_i.android.fragment;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bus_i.android.R;
import com.bus_i.android.adapter.RouteListAdapter;
import com.bus_i.android.asynctasks.CommonAsyncTask;
import com.bus_i.android.constants.AppConstants;
import com.bus_i.android.constants.WebConstants;
import com.bus_i.android.interfaces.AsyncTaskDelegate;
import com.bus_i.android.interfaces.SetMapRootInterface;
import com.bus_i.android.interfaces.SetMyLocationClickLstener;
import com.bus_i.android.manager.ParsingManager;
import com.bus_i.android.manager.PreferenceManager;
import com.bus_i.android.models.LocateBus;
import com.bus_i.android.models.Route;
import com.bus_i.android.models.Stop;
import com.bus_i.android.utills.GPSTracker;
import com.bus_i.android.utills.InternetUtil;
import com.bus_i.android.utills.MessageAlert;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
/**
 * 
 * @author Uma Shanker
 *
 */
public class MapViewFragment extends Fragment implements SetMyLocationClickLstener, AsyncTaskDelegate, OnRefreshListener<ListView>, OnMapLongClickListener,SetMapRootInterface,OnClickListener{
	private View rootView ;
	private Button mBtLIstView;
	private Button mBtMapView;
	private PullToRefreshListView mListViewRoout;
	private GoogleMap mGoogleMap;
	private MapView mMapView=null;
	private GPSTracker gps;
	private LatLng mUserLocationLatLng ;
	private Polyline polyline;
	private Marker mMarker;
	private LocateBus bus = null;
	private Timer locationTimer;
	private RouteListAdapter mRouteListAdapter;
	private List<Stop> stops;
	private boolean isBUsLocationUpdated = false;
	private CommonAsyncTask busLocCommand;

	/**
	 * initialize view and add listener
	 * @param rootView
	 */
	private void initView(){
		mListViewRoout = (PullToRefreshListView) getView().findViewById(R.id.lv_map_fragment_rolutelist);
		mListViewRoout.setOnRefreshListener(this);
		mMapView = (MapView) getView().findViewById(R.id.map);
		mBtLIstView=(Button)getView().findViewById(R.id.bt_map_fragment_rout_list);
		mBtLIstView.setOnClickListener(this);
		mBtMapView=(Button)rootView.findViewById(R.id.bt_map_fragment_rout_map);
		mBtMapView.setOnClickListener(this);
	}

	/**
	 * get Current location of the device other wise show alert
	 */
	private void init(){
		setUpMapIfNeeded();
		gps = new GPSTracker(getActivity());
		if(polyline == null){
			if(gps.canGetLocation()){
				mUserLocationLatLng=new LatLng(gps.getLatitude(), gps.getLongitude());
				ZoomAndSetMarker(mUserLocationLatLng, R.drawable.passenger_pin,AppConstants.MARKER_USER_LOCATION);

			}else{
				gps.showSettingsAlert();
			}
		}

		if(PreferenceManager.getInstance(getActivity()).isMap()){
			mMapView.setVisibility(View.VISIBLE);
			mListViewRoout.setVisibility(View.GONE);
			mBtMapView.setSelected(true);
			mBtLIstView.setSelected(false);
		}
		else{
			mMapView.setVisibility(View.GONE);
			mListViewRoout.setVisibility(View.VISIBLE);
			mBtLIstView.setSelected(true);
			mBtMapView.setSelected(false);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_map, container, false);
		return rootView;
	}




	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		initView();

		mMapView.onCreate(savedInstanceState);
		mMapView.onResume();

		init();
		initMap();

	}

	/**
	 * Initialize map and show current location of user
	 */
	private void initMap() {
		if (isGoogleMapsInstalled()) {

			mGoogleMap.setOnMapLongClickListener(this);
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage("Install Google Maps");
			builder.setCancelable(false);
			builder.setPositiveButton("Install",new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id=com.google.android.apps.maps"));
					startActivity(intent);
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	/**
	 * SetUp google map
	 */
	private void setUpMapIfNeeded() {
		if (mGoogleMap != null) {
			mGoogleMap = mMapView.getMap();
			return;
		}
		if (mGoogleMap == null) {
			mGoogleMap = mMapView.getMap();
			try {
				MapsInitializer.initialize(getActivity());
			} catch (GooglePlayServicesNotAvailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (mGoogleMap == null) {
			return;
		}
	}

	/**
	 * Check google Map is installed or not on device return true if installed otherwise return false
	 */
	public boolean isGoogleMapsInstalled() {
		try {
			getActivity().getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	/**
	 *  Add Marker on Map
	 */
	private void addMarker(LatLng latLngStart, int drawable, String markerName) {
		MarkerOptions startMarkerOption = new MarkerOptions().position(latLngStart).title(markerName);
		startMarkerOption.icon(BitmapDescriptorFactory.fromResource(drawable));
		mMarker=mGoogleMap.addMarker(startMarkerOption);
	}

	@Override
	public void onMapLongClick(LatLng arg0) {
		// TODO Auto-generated method stub
		// alarm trigger range
		addMarker(arg0,R.drawable.ic_alarm, AppConstants.MARKER_ALERM);
	}

	/**
	 * Reset the map for another search
	 */
	private void resetMap(){
		mGoogleMap.clear();

		//		if(mUserLocationLatLng != null && gps.canGetLocation()){
		//			mUserLocationLatLng=new LatLng(gps.getLatitude(), gps.getLongitude());
		//			ZoomAndSetMarker(mUserLocationLatLng, R.drawable.passenger_pin,AppConstants.MARKER_USER_LOCATION);
		//
		//		}else{
		//			//	gps.showSettingsAlert();
		//		}
	}

	@Override
	public void setMapRoot(LocateBus busData) {
		bus = busData;
		isBUsLocationUpdated = false;
		setMapRouteInfo(bus);
		setRouteListInfo(bus);
		getPeriodicLocation();
	}

	/**
	 * Set map and route view on response
	 * @param busData
	 */
	private void setMapRouteInfo(LocateBus busData) {
		resetMap();

		setFooterData(bus);
		if(bus != null && bus.getStatus().equals("success")){
			try{
				//set origin marker on map
				if(bus.getDetails().getOrigin() != null){
					addMarker(new LatLng(bus.getDetails().getOrigin().getLat(),bus.getDetails().getOrigin().getLng()),
							R.drawable.ic_origin_marker, AppConstants.MARKER_ORIGIN);
				}
				//set destination marker on map
				if(bus.getDetails().getDestination() != null){
					addMarker(new LatLng(bus.getDetails().getDestination().getLat(),bus.getDetails().getDestination().getLng()),
							R.drawable.ic_destination_marker_new, AppConstants.MARKER_DESTINATION);
				}
				drawRoute(bus.getRoute());

				LatLng latLang=new LatLng(bus.getLocation().getLat(),bus.getLocation().getLng());
				if(isBUsLocationUpdated){
					addMarker(latLang,R.drawable.ic_bus_marker_new,AppConstants.MARKER_CURRENT_POSITION);
				}else{
					ZoomAndSetMarker(latLang,R.drawable.ic_bus_marker_new,AppConstants.MARKER_CURRENT_POSITION);
				}
			}catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
		else{
			resetFooter();
			MessageAlert.showGeneralAlert(getActivity(), getResources().getString(R.string.alert), getResources().getString(R.string.bus_not_found), getResources().getDrawable(R.drawable.ic_launcher), false);
		}
	}
	/**
	 * reset footer if bus not found in any case
	 */
	private void resetFooter(){
		ImageView signalQuality = (ImageView)getView().findViewById(R.id.imageSignalQuality);
		signalQuality.setVisibility(View.VISIBLE);
		TextView busName = (TextView)getView().findViewById(R.id.statusTitle);
		busName.setText(getResources().getString(R.string.status_title_initial));
		TextView busSpeed =  (TextView)getView().findViewById(R.id.statusSpeed);
		busSpeed.setText(getResources().getString(R.string.status_speed));
		TextView busTime =  (TextView)getView().findViewById(R.id.statusLastTime);
		busTime.setText(getResources().getString(R.string.status_last_time));
		TextView busAddress =  (TextView)getView().findViewById(R.id.statusLastStop);
		busAddress.setText(getResources().getString(R.string.status_last_stop));
		TextView busDestination =  (TextView)getView().findViewById(R.id.statusDestName);
		busDestination.setText(getResources().getString(R.string.status_next_stop));
	}
	/**
	 * setBus detail on footer
	 * @param bus
	 */
	private void setFooterData(LocateBus bus){
		try{
			ImageView signalQuality = (ImageView)getView().findViewById(R.id.imageSignalQuality);
			signalQuality.setVisibility(View.GONE);
			TextView busName = (TextView)getView().findViewById(R.id.statusTitle);
			busName.setText(bus.getDetails().getName());
			TextView busSpeed =  (TextView)getView().findViewById(R.id.statusSpeed);
			if(bus.getLocation().getSpeed() > 0){
				busSpeed.setText(bus.getLocation().getSpeed() +"km/h");
			}
			TextView busTime =  (TextView)getView().findViewById(R.id.statusLastTime);
			busTime.setText(getTime(bus.getTimestamp()));
			TextView busAddress =  (TextView)getView().findViewById(R.id.statusLastStop);
			busAddress.setText((bus.getAddress().getLine1() + bus.getAddress().getLine2()).replace("null", ""));
			TextView busDestination =  (TextView)getView().findViewById(R.id.statusDestName);
			if(bus.getDetails().getDestination() != null){
				busDestination.setText(bus.getDetails().getDestination().getName());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * get time from timestamp
	 * @param time
	 * @return
	 */
	private String getTime(long time) {
		Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		cal.setTimeInMillis(time);
		String ltime = DateFormat.format("hh:mm a", cal).toString();
		return ltime;
	}
	/**
	 * Zoom and animate camera on location and add bus marker
	 */
	private void ZoomAndSetMarker(LatLng latlng, int icon, String markerName){
		//		GPSTracker	gps = new GPSTracker(getActivity());
		//		if(latlng != null && gps.canGetLocation()){

		mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom((latlng), 11.0f));
		addMarker(latlng,icon,markerName);
		//		}else{
		//			//gps.showSettingsAlert();
		//		}


	}


	/**
	 * This will draw a particular route from the route list we got from response Api
	 * @param pos
	 */
	public void drawRoute(List<Route> route) {

		if(polyline != null){
			polyline.remove();
		}
		PolylineOptions rectLine = new PolylineOptions().width(5).color(getResources().getColor(R.color.blue_text_color));
		for (int i = 0; i < route.size(); i++) {
			rectLine.add(new LatLng(route.get(i).getLat(),route.get(i).getLng()));
		}
		polyline = mGoogleMap.addPolyline(rectLine);

	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.bt_map_fragment_rout_list:
			if(!mBtLIstView.isSelected()){
				mMapView.setVisibility(View.GONE);
				mListViewRoout.setVisibility(View.VISIBLE);
				mBtLIstView.setSelected(true);
				mBtMapView.setSelected(false);
			}
			break;
		case R.id.bt_map_fragment_rout_map:

			if(!mBtMapView.isSelected()){
				mMapView.setVisibility(View.VISIBLE);
				mListViewRoout.setVisibility(View.GONE);
				mBtLIstView.setSelected(false);
				mBtMapView.setSelected(true);
			}
			break;

		default:
			break;
		}

	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		// this will load the last search bus name if it is not null
		if(InternetUtil.isInternetOn(getActivity())){
			if(PreferenceManager.getInstance(getActivity()).getSearchedBusName() != null){
				searchRunningBus(PreferenceManager.getInstance(getActivity()).getSearchedBusName());
			}
			else{
				MessageAlert.showGeneralAlert(getActivity(), getResources().getString(R.string.alert), getResources().getString(R.string.bus_not_found), getResources().getDrawable(R.drawable.ic_launcher), false);

			}
		}
		else{
			mListViewRoout.onRefreshComplete();
			InternetUtil.showErrorDialog(getActivity(), false);
		}

	}

	/**
	 * search bus using bus name and return on map
	 * @param query
	 */
	private void searchRunningBus(final String query) {
		if(InternetUtil.isInternetOn(getActivity())){
			String url=WebConstants.LOCATE_BUS+"cmd=locate&code="+query;
			busLocCommand =	new CommonAsyncTask(getActivity(),this, false);
			busLocCommand.execute(url);
		}else{
			InternetUtil.showErrorDialog(getActivity(), false);
		}
	}

	/**
	 * Set adapter for route list view
	 * @param bus
	 */
	private void setRouteListInfo(LocateBus bus) {
		if(bus != null && bus.getStops() != null){
			if(stops != null){
				stops.clear();
				stops.addAll(bus.getStops());
			}else{
				stops = bus.getStops();
			}
			if(mRouteListAdapter == null){
				mRouteListAdapter = new RouteListAdapter(stops,getActivity());
				mListViewRoout.setAdapter(mRouteListAdapter);
			}
			else{
				mRouteListAdapter.notifyDataSetChanged();
			}
		}
	} 

	@Override
	public void setResponse(LocateBus response) {
		mListViewRoout.onRefreshComplete();
		if(response !=null){
			bus = response;//ParsingManager.doParsing(response, LocateBus.class);
			isBUsLocationUpdated = true;
			setMapRouteInfo(bus);
			setRouteListInfo(bus);
		}

	}



	@Override
	public void setIsMyLocation(Boolean isMyloaction) {
		if(isMyloaction == false){

			mUserLocationLatLng=new LatLng(gps.getLatitude(), gps.getLongitude());
			if(mUserLocationLatLng != null){
				ZoomAndSetMarker(mUserLocationLatLng, R.drawable.passenger_pin,AppConstants.MARKER_USER_LOCATION);
			}
		}else{

			if(bus != null && bus.getStatus().equals("success")){
				LatLng latLang=new LatLng(bus.getLocation().getLat(),bus.getLocation().getLng());
				ZoomAndSetMarker(latLang,R.drawable.ic_bus_marker_new,AppConstants.MARKER_CURRENT_POSITION);

			}
			else{
				MessageAlert.showGeneralAlert(getActivity(), getResources().getString(R.string.alert),bus.getMessage(), getResources().getDrawable(R.drawable.ic_launcher), false);
			}
		}
	}

	/**
	 * this method will get the updated bus location periodically with period of two second
	 */

	public void getPeriodicLocation() {
		final Handler handler = new Handler();
		locationTimer = new Timer();
		TimerTask doAsynchronousTask = new TimerTask() {       
			@Override
			public void run() {
				handler.post(new Runnable() {

					public void run() {       
						try {
							if(busLocCommand == null ){
								busLocCommand =new CommonAsyncTask(getActivity(),MapViewFragment.this, false);
								if(PreferenceManager.getInstance(getActivity()).getSearchedBusName() != null){
									busLocCommand.execute(WebConstants.LOCATE_BUS+"cmd=locate&code="+PreferenceManager.getInstance(getActivity()).getSearchedBusName());
								}
							}else if(busLocCommand.getStatus().equals(AsyncTask.Status.FINISHED) || busLocCommand.getStatus().equals(AsyncTask.Status.PENDING)){
								busLocCommand =new CommonAsyncTask(getActivity(),MapViewFragment.this, false);
								if(PreferenceManager.getInstance(getActivity()).getSearchedBusName() != null){
									busLocCommand.execute(WebConstants.LOCATE_BUS+"cmd=locate&code="+PreferenceManager.getInstance(getActivity()).getSearchedBusName());
								}
							}
							Log.e("GetBusLocation", "Get Bus Updated Location");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		};

		if(InternetUtil.isInternetOn(getActivity())){
			locationTimer.schedule(doAsynchronousTask, 0, 30000); //execute in every 30 sec.
		}else{
			InternetUtil.showErrorDialog(getActivity(), false);
		}
	}

	/**
	 * stop the timer if  needed
	 */
	public void stopPeriodicLocation(){
		if(locationTimer != null){
			locationTimer.cancel();
		}
		if(busLocCommand != null && busLocCommand.getStatus() == AsyncTask.Status.RUNNING){
			busLocCommand.cancel(true);
		}
	}
	@Override
	public void onStart() {
		super.onStart();
		//	getPeriodicLocation();
	}
	@Override
	public void onStop() {
		stopPeriodicLocation();
		if(busLocCommand != null && busLocCommand.getStatus() == AsyncTask.Status.RUNNING){
			busLocCommand.cancel(true);
		}
		
		super.onStop();
	}
	
	@Override
	public void onPause() {
		stopPeriodicLocation();
		if(busLocCommand != null && busLocCommand.getStatus() == AsyncTask.Status.RUNNING){
			busLocCommand.cancel(true);
		}
		
		super.onPause();
	}
}


