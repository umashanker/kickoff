package com.bus_i.android.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.bus_i.android.constants.AppConstants;
import com.bus_i.android.constants.PreferenceManagerConstants;

public class PreferenceManager {
	private static PreferenceManager instance;
	private SharedPreferences sharedPreferences;

	private PreferenceManager(Context context){
		sharedPreferences = context.getSharedPreferences(PreferenceManagerConstants.SETTING_PREFERENCE, 0);
	}

	public static PreferenceManager getInstance(Context context){
		if (instance == null) {
			instance = new PreferenceManager(context);
		}
		return instance;
	}

	public boolean isMap() {
		return sharedPreferences.getBoolean(PreferenceManagerConstants.DEFAULT_APP_MODE, true);
	}

	public void setDefaultAppModeMap(boolean isMap) {
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putBoolean(PreferenceManagerConstants.DEFAULT_APP_MODE, isMap);
		editor.commit();
	}
	public void setSearchedBusName(String busName){
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putString(PreferenceManagerConstants.LAST_SEARCHED_BUS_NAME, busName);
		editor.commit();
	}
	public String getSearchedBusName(){
		return sharedPreferences.getString(PreferenceManagerConstants.LAST_SEARCHED_BUS_NAME, null);
	}
	public void setappLaunchedFirstTime(Boolean applaunched){
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putBoolean(PreferenceManagerConstants.IS_APP_LAUNCHED_FIRST_TIME, applaunched);
		editor.commit();
	}
	public Boolean getappLaunchedFirstTime(){
		return sharedPreferences.getBoolean(PreferenceManagerConstants.IS_APP_LAUNCHED_FIRST_TIME, true);
	}
}
