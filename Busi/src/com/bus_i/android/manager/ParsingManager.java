package com.bus_i.android.manager;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ParsingManager {

	public static <T> T doParsing(String response, Class<T> mappTo){
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(response, mappTo);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
