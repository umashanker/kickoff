
package com.bus_i.android.models;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Route {

	private Long alt;
	private Double lng;
	private Double lat;
	private Long bearing;
	private Long speed;
	private Long accuracy;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Long getAlt() {
		return alt;
	}

	public void setAlt(Long alt) {
		this.alt = alt;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Long getBearing() {
		return bearing;
	}

	public void setBearing(Long bearing) {
		this.bearing = bearing;
	}

	public Long getSpeed() {
		return speed;
	}

	public void setSpeed(Long speed) {
		this.speed = speed;
	}

	public Long getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Long accuracy) {
		this.accuracy = accuracy;
	}



	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
