
package com.bus_i.android.models;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Stop {

    private String state;
    private Location_ location;
    private Address address;
    private Long timestamp;
    private String infoHtml;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Location_ getLocation() {
        return location;
    }

    public void setLocation(Location_ location) {
        this.location = location;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getInfoHtml() {
        return infoHtml;
    }

    public void setInfoHtml(String infoHtml) {
        this.infoHtml = infoHtml;
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
