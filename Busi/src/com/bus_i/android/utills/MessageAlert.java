package com.bus_i.android.utills;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.widget.Toast;

import com.bus_i.android.R;

/**
 * 
 * @author Uma Shanker
 *
 */
public class MessageAlert {

	/*
	 * Show the toast on screen with specified location
	 */
	public static void showToast(String msg, Context mContext) {

		Toast toast = Toast.makeText(mContext, msg, Toast.LENGTH_LONG);

		toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 200);

		toast.setDuration(Toast.LENGTH_LONG);

		toast.show();
	}

	/**
	 * Show Simple Alert message 
	 * @param mContext
	 * @param spanned
	 * on ok click just dismiss the dialog
	 */
	public static void showGeneralAlert(final Activity mContext, String title,String msg,Drawable icon,final Boolean isBack) {

		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mContext);
		alertBuilder.setTitle(title);
		alertBuilder.setMessage(msg);
		alertBuilder.setIcon(icon);

		alertBuilder.setPositiveButton(mContext.getResources().getString(R.string.button_ok),new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(isBack){
					mContext.finish();
				}
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertBuilder.create();
		alertDialog.show();
	}
}
