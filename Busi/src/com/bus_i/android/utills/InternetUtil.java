package com.bus_i.android.utills;

//
//import android.content.Context;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.util.Log;
//
//public class InternetUtil {
//
//	private static final String TAG = InternetUtil.class.getName();
//
//	/**
//	 * Check Internet connectivity
//	 *     
//	 * @return
//	 */
//	public static boolean isInternetConnectivityAvailable(Context ctx) {
//
//		ConnectivityManager cm = (ConnectivityManager) ctx
//				.getSystemService(Context.CONNECTIVITY_SERVICE);
//		NetworkInfo netInfo = cm.getActiveNetworkInfo();
//
//		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
//			return true;
//		}
//		return false;
//	}
//
//	public static boolean isConnectionAvailable(Context ctx) {
//
//		try {
//			ConnectivityManager connec = (ConnectivityManager) ctx
//					.getSystemService(Context.CONNECTIVITY_SERVICE);
//
//			if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
//					|| connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING
//					|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING
//					|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
//
//				return true;
//
//			} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
//					|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {
//
//				return false;
//			}
//		} catch (Exception e) {
//			Log.e(TAG, "Exception @ isConnectionAvailable: " + e.toString());
//		}
//
//		return false;
//	}
//}

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.view.Window;

import com.bus_i.android.R;
/**
 * 
 * @author Uma Shanker
 *
 */
public class InternetUtil {

	// CHECK FOR INTERNET METHOD

	public static boolean isInternetOn(Context context) {

		// Log.d(TAG, "In isInternetOn method");

		ConnectivityManager connec = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		// ARE WE CONNECTED TO THE NET

		if (connec.getActiveNetworkInfo() != null && connec.getActiveNetworkInfo().isConnected()) { 
			
			return true; 
			
		} else {
			
			return false;
		}

	}

	/**
	 * Display error dialog
	 * 
	 * @param context
	 * @param isGoBack
	 *            - if true go to the previous screen when clicked on OK
	 */
	public static void showErrorDialog(final Context context,
			final boolean isGoBack) {

		String alertMessage = context.getResources().getString(R.string.network_error);

		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		alertDialog.setTitle(context.getResources().getString(R.string.error));
		alertDialog.setMessage(alertMessage);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,context.getResources().getString(R.string.button_ok), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {

				if (isGoBack) {
					((Activity) context).finish();
				}
				return;
			}
		});

		alertDialog.show();
	}

}
